<?php

use Illuminate\Database\Seeder;

class ContatoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'endereco' => 'Av. Angélica, 2118, 12º andar • 01228 200 • São Paulo • SP',
            'telefone' => '11 3138-5901 • Ramal 5924',
            'email'    => 'contato@mokainvest.com.br',
        ]);
    }
}
