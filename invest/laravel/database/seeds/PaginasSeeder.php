<?php

use Illuminate\Database\Seeder;

class PaginasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('paginas')->insert([
            [
                'slug'   => 'moka-invest',
                'titulo' => 'Moka Invest',
                'texto'  => 'texto',
            ],
            [
                'slug'   => 'produtos',
                'titulo' => 'Produtos',
                'texto'  => 'texto',
            ],
            [
                'slug'   => 'fidc',
                'titulo' => 'FIDC',
                'texto'  => 'texto',
            ],
            [
                'slug'   => 'atendimento',
                'titulo' => 'Atendimento',
                'texto'  => 'texto',
            ],
        ]);
    }
}
