(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.bannersHome = function() {
        var $wrapper = $('.banners');
        if (!$wrapper.length) return;

        $wrapper.cycle({
            slides: '>.banner',
        });
    };

    App.mobileToggle = function() {
        var $handle = $('#mobile-toggle'),
            $nav    = $('nav#mobile');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();
            $nav.slideToggle();
            $handle.toggleClass('close');
        });
    };

    App.init = function() {
        this.bannersHome();
        this.mobileToggle();
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
