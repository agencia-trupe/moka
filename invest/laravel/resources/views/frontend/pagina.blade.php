@extends('frontend.common.template')

@section('content')

    <div class="center">
        <div class="main">
            <h1>{{ $pagina->titulo }}</h1>

            @if($pagina->slug === 'atendimento')
            <h2>{{ config('site.name') }}</h2>
            <ul>
                <li>Fone: {{ $contato->telefone }}</li>
                <li>E-mail: <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a></li>
                <li>Endereço: {{ $contato->endereco }}</li>
            </ul>
            @endif

            {!! $pagina->texto !!}
        </div>
    </div>

@endsection