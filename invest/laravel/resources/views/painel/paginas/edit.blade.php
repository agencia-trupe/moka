@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Editar Página: {{ $pagina->titulo }}</h2>
    </legend>

    {!! Form::model($pagina, [
        'route'  => ['painel.paginas.update', $pagina->id],
        'method' => 'patch'])
    !!}

    @include('painel.paginas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
