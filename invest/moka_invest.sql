-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: 17-Nov-2015 às 17:16
-- Versão do servidor: 5.5.38
-- PHP Version: 5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `moka_invest`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE `banners` (
`id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frase` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `imagem`, `frase`, `created_at`, `updated_at`) VALUES
(1, 0, '20151117160753_banner1.jpg', '', '2015-11-17 18:07:56', '2015-11-17 18:07:56'),
(2, 1, '20151117160812_banner3.jpg', 'Transparência nas operações', '2015-11-17 18:08:12', '2015-11-17 18:08:12'),
(3, 2, '20151117160833_banner2.jpg', 'Soluções de crédito sob medida', '2015-11-17 18:08:33', '2015-11-17 18:08:33');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
`id` int(10) unsigned NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `endereco`, `telefone`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Av. Angélica, 2118, 12º andar • 01228 200 • São Paulo • SP', '11 3138-5901 • Ramal 5924', 'contato@mokainvest.com.br', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_11_16_131444_create_banners_table', 1),
('2015_11_16_131449_create_paginas_table', 1),
('2015_11_17_130528_create_contato_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `paginas`
--

CREATE TABLE `paginas` (
`id` int(10) unsigned NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `paginas`
--

INSERT INTO `paginas` (`id`, `slug`, `titulo`, `texto`, `created_at`, `updated_at`) VALUES
(1, 'moka-invest', 'Moka Invest', '<p>Desde setembro de 2000, a Moka atua no mercado de an&aacute;lise e concess&atilde;o de&nbsp;cr&eacute;dito, otimizandoo fluxo de caixa para empresas de portes diversos.</p>\r\n\r\n<p>A Moka conta com processos eletr&ocirc;nicos de certifica&ccedil;&atilde;o digital, assim como&nbsp;processos de an&aacute;lise de cr&eacute;dito.</p>\r\n\r\n<p>Ap&oacute;s anos de experi&ecirc;ncia e parcerias, a Moka &eacute; hoje reconhecida pela seriedade no&nbsp;relacionamento com seus clientes e credibilidade nas suas opera&ccedil;&otilde;es.</p>\r\n\r\n<p><img src="http://mokainvest.dev/assets/img/paginas/20151117161128_invest.png" /></p>\r\n', '0000-00-00 00:00:00', '2015-11-17 18:11:29'),
(2, 'produtos', 'Produtos', '<ul>\r\n	<li><strong>Antecipa&ccedil;&atilde;o de receb&iacute;veis de cr&eacute;dito</strong><br />\r\n	Duplicatas, cheques e contratos.</li>\r\n	<li><strong>An&aacute;lise de cr&eacute;dito</strong><br />\r\n	An&aacute;lise de cr&eacute;dito dos seus clientes (correntes e prospects) utilizando ferramentas exclusivas, conferindo&nbsp;seguran&ccedil;a no dia a dia de suas opera&ccedil;&otilde;es e em futuros neg&oacute;cios.</li>\r\n	<li><strong>Cobran&ccedil;a dos t&iacute;tulos</strong><br />\r\n	Cobran&ccedil;a dos t&iacute;tulos descontados ou cobran&ccedil;a simples de todo o contas a receber em todo o territ&oacute;rio nacional.</li>\r\n</ul>\r\n', '0000-00-00 00:00:00', '2015-11-17 18:12:45'),
(3, 'fidc', 'FIDC', '<p>Os FIDCs, Fundo de Investimento em Direitos Credit&oacute;rios, foram criados pelo Banco Central do Brasil em 2001. O objetivo&nbsp;da cria&ccedil;&atilde;o desses fundos &eacute; de as empresas venderem seus direitos credit&oacute;rios (receb&iacute;veis) origin&aacute;rios nos mais&nbsp;diversos segmentos do mercado (comercial, imobili&aacute;rio, financeiro, etc). Com isso, as empresas passam a ter mais uma&nbsp;fonte de capta&ccedil;&atilde;o de recursos a custos atraentes e mais flexibilidade e agilidade.</p>\r\n\r\n<p>Em 2011, foi criado o Moka Fund I FIDC para adquirir os receb&iacute;veis de seus clientes. O Moka Fund I possui uma&nbsp;regulamenta&ccedil;&atilde;o pr&oacute;pria 100% aprovada e fiscalizada pela CVM - Comiss&atilde;o de Valores Mobili&aacute;rios.</p>\r\n\r\n<p><a href="#">Saiba mais sobre Moka Fund I</a></p>\r\n', '0000-00-00 00:00:00', '2015-11-17 18:13:33'),
(4, 'atendimento', 'Atendimento', '<h1>Perguntas Frequentes</h1>\r\n\r\n<ul>\r\n	<li><strong>Quais receb&iacute;vies s&atilde;o eleg&iacute;veis para desconto?</strong><br />\r\n	Duplicatas, Contratos e Cheques</li>\r\n	<li><strong>Posso ser cliente, mesmo tendo algum apontamento em bureau de cr&eacute;dito?</strong><br />\r\n	Sim. Uma visita comercial seguida de uma avalia&ccedil;&atilde;o de cr&eacute;dito s&atilde;o feitas antes do inicio formal do relacionamento. E&nbsp;apontamentos n&atilde;o s&atilde;o determinantes mas sim componentes da avalia&ccedil;&atilde;o.</li>\r\n	<li><strong>A Moka s&oacute; atende clientes em S&atilde;o Paulo ou pode ser de qualquer lugar do Brasil?</strong><br />\r\n	A Moka n&atilde;o tem limita&ccedil;&atilde;o de pra&ccedil;a. Atendemos clientes em todo territ&oacute;rio nacional.</li>\r\n	<li><strong>Existe um faturamento m&iacute;nimo mensal para poder fazer o desconto?</strong><br />\r\n	Sim. Focamos em empresas com faturamento acima de R$ 500 mil/m&ecirc;s.</li>\r\n	<li><strong>Existe algum segmento ou setor que a Moka n&atilde;o atende?</strong><br />\r\n	N&atilde;o. Atendemos todos os segmentos da Ind&uacute;stria, Servi&ccedil;o e Com&eacute;rcio.</li>\r\n	<li><strong>Quem &eacute; o contato dentro da Moka?</strong><br />\r\n	Todo cliente Moka tem um gerente espec&iacute;fico respons&aacute;vel pelo relacionamento. Al&eacute;m disso, o cliente tem acesso a mesa do&nbsp;suporte comercial para tirar todas as d&uacute;vidas que possam surgir durante a opera&ccedil;&atilde;o.</li>\r\n	<li><strong>Quanto tempo demora para a abertura do relacionamento?</strong><br />\r\n	Com todos os documentos necess&aacute;rios enviados &agrave; Moka, o contrato de relacionamento entre as partes - Contrato de Cess&atilde;o de&nbsp;Cr&eacute;dito - &eacute; confeccionado em at&eacute; 48 horas.</li>\r\n	<li><strong>O cr&eacute;dito &eacute; feito no mesmo dia?</strong><br />\r\n	Sim. Para opera&ccedil;&otilde;es que forem enviadas at&eacute; 15:30 contendo todas as informa&ccedil;&otilde;es necess&aacute;rias, o cr&eacute;dito &eacute; feito no mesmo dia.</li>\r\n	<li><strong>Quais informa&ccedil;&otilde;es s&atilde;o necess&aacute;rias para a opera&ccedil;&atilde;o ser feita?</strong><br />\r\n	Envio do arquivo XML das Notas Fiscais e dados cadastrais completos (incu&iacute;ndo telefone) dos sacados.</li>\r\n	<li><strong>Como a opera&ccedil;&atilde;o &eacute; formalizada?</strong><br />\r\n	Atrav&eacute;s de assinatura digital no portal de assinaturas oferecido pela Moka. &Eacute; necess&aacute;rio que a pessoa respons&aacute;vel pela assinatura&nbsp;tenha E-Cpf.</li>\r\n	<li><strong>Quais informa&ccedil;&otilde;es s&atilde;o enviadas antes da assinatura da opera&ccedil;&atilde;o?</strong><br />\r\n	A Moka envia um relat&oacute;rio contendo: todos os t&iacute;tulos a serem cedidos, valor bruto, des&aacute;gio, depesas pendentes e valor l&iacute;quido. A&nbsp;transper&ecirc;ncia &eacute; asboluta e um dos pontos fortes da Moka.</li>\r\n	<li><strong>Qual o hor&aacute;rio de funcionamento?</strong><br />\r\n	Das 9:00 &agrave;s 17:00. De segunda a sexta, exceto feriados nacional e fer&iacute;ados municipal (pra&ccedil;a de S&atilde;o Paulo).</li>\r\n</ul>\r\n', '0000-00-00 00:00:00', '2015-11-17 18:15:39');

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$stFAjQ1bVytRKww5GELlz.wo0EJxmCNmQ00QoeYCFzLhO19uc.UPy', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paginas`
--
ALTER TABLE `paginas`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `paginas_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
 ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `paginas`
--
ALTER TABLE `paginas`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
