-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: 17-Nov-2015 às 18:01
-- Versão do servidor: 5.5.38
-- PHP Version: 5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `moka_gestora`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `arquivos`
--

CREATE TABLE `arquivos` (
`id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `arquivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE `banners` (
`id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frase` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `imagem`, `frase`, `created_at`, `updated_at`) VALUES
(1, 0, '20151117165420_banner1.jpg', '', '2015-11-17 18:54:22', '2015-11-17 18:54:22'),
(2, 1, '20151117165440_banner3.jpg', 'Soluções de investimento e gestão', '2015-11-17 18:54:40', '2015-11-17 18:54:40'),
(3, 2, '20151117165549_banner4.jpg', 'Know-how e parceria', '2015-11-17 18:55:50', '2015-11-17 18:55:50');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
`id` int(10) unsigned NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `endereco`, `telefone`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Av. Angélica, 2118, 12º andar • 01228 200 • São Paulo • SP', '11 3138-5901 • Ramal 5924', 'contato@mokagestora.com.br', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_11_16_131444_create_banners_table', 1),
('2015_11_16_131449_create_paginas_table', 1),
('2015_11_17_130528_create_contato_table', 1),
('2015_11_17_162459_create_arquivos_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `paginas`
--

CREATE TABLE `paginas` (
`id` int(10) unsigned NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `paginas`
--

INSERT INTO `paginas` (`id`, `slug`, `titulo`, `texto`, `created_at`, `updated_at`) VALUES
(1, 'moka-gestora', 'Moka Gestora', '<p>Criada em 2014 com o intuito de adequar a opera&ccedil;&atilde;o de FIDC &agrave;s novas determina&ccedil;&otilde;es da CVM,&nbsp;a Moka Gestora tem como objetivo, al&eacute;m de oferecer servi&ccedil;os de gest&atilde;o financeira, consultoria&nbsp;negocial, econ&ocirc;mica e financeira.</p>\r\n\r\n<p>Aproveitando a expertise no mercado de receb&iacute;veis do grupo Moka, bem como a capacidade de&nbsp;negocia&ccedil;&atilde;o de conflito e an&aacute;lise econ&ocirc;mica / financeira de empresas, oferecemos em nosso portf&oacute;lio os&nbsp;seguintes servi&ccedil;os:</p>\r\n\r\n<ul>\r\n	<li><strong>Estrutura&ccedil;&atilde;o de ve&iacute;culos de negocia&ccedil;&atilde;o de receb&iacute;veis</strong></li>\r\n	<li><strong>Gest&atilde;o para FIDCs</strong></li>\r\n	<li><strong>Reestrutura&ccedil;&atilde;o Financeira e Organizacional</strong></li>\r\n	<li><strong>Capta&ccedil;&atilde;o e apoio a enture capital</strong></li>\r\n</ul>\r\n', '0000-00-00 00:00:00', '2015-11-17 18:56:27'),
(2, 'fundos', 'Fundos', '<p>As informa&ccedil;&otilde;es contidas nesse material s&atilde;o de car&aacute;ter exclusivamente informativo. Fundos de Investimentos n&atilde;o contam&nbsp;com a garantia do administrador, do gestor de carteira, de qualquer mecanismo de seguro ou, ainda, do Fundo Garantidor&nbsp;de Cr&eacute;ditos - FGC. A rentabilidade passada n&atilde;o representa garantia de rentabilidade futura. Ao investidor &eacute; recomendada a&nbsp;leitura cuidadosa dos prospectos e dos regulamentos dos Fundos de Investimentos ao aplicar seus recursos. Os Fundos&nbsp;utilizam estrat&eacute;gias com derivativos como parte integrante de suas pol&iacute;ticas de investimento, as quais, da forma como s&atilde;o&nbsp;adotadas, podem resultar em significativas perdas patrimoniais para seus cotistas. Os Fundos est&atilde;o autorizados a realizar&nbsp;aplica&ccedil;&otilde;es em ativos financeiros no exterior.</p>\r\n', '0000-00-00 00:00:00', '2015-11-17 18:57:02'),
(3, 'relatorios', 'Relatórios', '<p><strong>Relat&oacute;rios CVM</strong></p>\r\n\r\n<p>Em desenvolvimento a fim de atender as exig&ecirc;ncias da CVM previstas para entrar&nbsp;em funcionamento em Julho 2016</p>\r\n', '0000-00-00 00:00:00', '2015-11-17 18:57:20'),
(4, 'atendimento', 'Atendimento', '<h1>Disclosure</h1>\r\n\r\n<p><strong>As informa&ccedil;&otilde;es no presente material s&atilde;o exclusivamente informativas. Rentabilidade passada n&atilde;o representa garantia de&nbsp;rentabilidade futura. Ao investidor &eacute; recomendada a leitura cuidadosa do prospecto e do regulamento dos fundos de investimento&nbsp;ao aplicar seus recursos. Fundos de investimentos n&atilde;o contam com a garantia do administrador, do gestor, do consultor de cr&eacute;dito&nbsp;ou ainda do fundo garantidor de cr&eacute;dito - FGC. Este fundo utiliza t&iacute;tulos privados de cr&eacute;dito como parte integrante de sua pol&iacute;tica&nbsp;de investimento. Tal estrat&eacute;gia pode resultar em significativas perdas patrimoniais para seus cotistas. Para avalia&ccedil;&atilde;o da&nbsp;performance de um FI, &eacute; recomend&aacute;vel uma an&aacute;lise de no m&iacute;nimo doze meses. Leia o prospecto antes de aceitar a oferta.</strong></p>\r\n\r\n<p><strong><img src="http://mokagestora.dev/assets/img/paginas/20151117165958_disclosure.png" /></strong></p>\r\n', '0000-00-00 00:00:00', '2015-11-17 18:59:59');

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$/fvJQNoXNcoFZ/0nY5xZC.zk33EiP89mguDrs.mJdYYLBoFZ4jU.S', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `arquivos`
--
ALTER TABLE `arquivos`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paginas`
--
ALTER TABLE `paginas`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `paginas_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
 ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `arquivos`
--
ALTER TABLE `arquivos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `paginas`
--
ALTER TABLE `paginas`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
