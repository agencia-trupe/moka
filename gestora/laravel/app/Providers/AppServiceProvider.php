<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('frontend.*', function($view) {
            $view->with('paginas', \App\Models\Pagina::orderBy('id', 'ASC')->select('titulo', 'slug')->get());
            $view->with('contato', \App\Models\Contato::first());
            $view->with('arquivos', \App\Models\Arquivo::ordenados()->get());
            $view->with('relatorios', \App\Models\Relatorio::ordenados()->get());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
