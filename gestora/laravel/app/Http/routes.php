<?php

// Painel
Route::group(['prefix' => 'painel', 'namespace' => 'Painel', 'middleware' => 'auth'], function() {
    Route::get('/', ['as' => 'painel', 'uses' => 'HomeController@index']);
    Route::resource('banners', 'BannersController');
    Route::resource('paginas', 'PaginasController');
    Route::resource('arquivos', 'ArquivosController');
    Route::resource('relatorios', 'RelatoriosController');
    Route::resource('contato', 'ContatoController');
    Route::resource('usuarios', 'UsuariosController');
    Route::post('order', 'HomeController@order');
    Route::post('ckeditor-upload', 'PaginasController@imageUpload');
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});


// Auth
Route::group(['prefix' => 'painel', 'namespace' => 'Auth'], function() {
    Route::get('login', ['as' => 'login', 'uses' => 'AuthController@getLogin']);
    Route::post('login', ['as' => 'auth', 'uses' => 'AuthController@postLogin']);
    Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@getLogout']);
});


Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('{pagina_slug}', ['as' => 'pagina', 'uses' => 'HomeController@pagina']);
