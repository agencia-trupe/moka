<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests\RelatoriosRequest;

use App\Http\Controllers\Controller;

use App\Models\Relatorio;

class RelatoriosController extends Controller
{
    public function index()
    {
        $relatorios = Relatorio::ordenados()->get();

        return view('painel.relatorios.index', compact('relatorios'));
    }

    public function create()
    {
        return view('painel.relatorios.create');
    }

    public function uploadRelatorio($file) {
        $filePath = 'assets/relatorios/';
        $fileName = preg_replace("([^\w\s\d\-_~,;:\[\]\(\).])", '', $file->getClientOriginalName());
        $fileName = date('YmdHis').'_'.$fileName;

        $file->move(public_path($filePath), $fileName);
        return $fileName;
    }

    public function store(RelatoriosRequest $request)
    {
        try {
            $input = $request->all();
            $input['arquivo'] = $this->uploadRelatorio($input['arquivo']);

            Relatorio::create($input);

            return redirect()->route('painel.relatorios.index')->with('success', 'Relatório adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar relatório: '.$e->getMessage()]);

        }
    }

    public function edit(Relatorio $relatorio)
    {
        return view('painel.relatorios.edit', compact('relatorio'));
    }

    public function update(Relatorio $relatorio, RelatoriosRequest $request)
    {
        try {

            $input = $request->all();

            if ($request->hasFile('arquivo')) {
                $input['arquivo'] = $this->uploadRelatorio($input['arquivo']);
            } else {
                unset($input['arquivo']);
            }

            $relatorio->update($input);

            return redirect()->route('painel.relatorios.index')->with('success', 'Relatório alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar relatório: '.$e->getMessage()]);

        }
    }

    public function destroy(Relatorio $relatorio)
    {
        try {

            $relatorio->delete();
            return redirect()->route('painel.relatorios.index')
                             ->with('success', 'Relatório excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir relatório: '.$e->getMessage()]);

        }
    }
}
