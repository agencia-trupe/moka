<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests\ArquivosRequest;

use App\Http\Controllers\Controller;

use App\Models\Arquivo;

class ArquivosController extends Controller
{
    public function index()
    {
        $arquivos = Arquivo::ordenados()->get();

        return view('painel.arquivos.index', compact('arquivos'));
    }

    public function create()
    {
        return view('painel.arquivos.create');
    }

    public function uploadArquivo($file) {
        $filePath = 'assets/fundos/';
        $fileName = preg_replace("([^\w\s\d\-_~,;:\[\]\(\).])", '', $file->getClientOriginalName());
        $fileName = date('YmdHis').'_'.$fileName;

        $file->move(public_path($filePath), $fileName);
        return $fileName;
    }

    public function store(ArquivosRequest $request)
    {
        try {
            $input = $request->all();
            $input['arquivo'] = $this->uploadArquivo($input['arquivo']);

            Arquivo::create($input);

            return redirect()->route('painel.arquivos.index')->with('success', 'Arquivo adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar arquivo: '.$e->getMessage()]);

        }
    }

    public function edit(Arquivo $arquivo)
    {
        return view('painel.arquivos.edit', compact('arquivo'));
    }

    public function update(Arquivo $arquivo, ArquivosRequest $request)
    {
        try {

            $input = $request->all();

            if ($request->hasFile('arquivo')) {
                $input['arquivo'] = $this->uploadArquivo($input['arquivo']);
            } else {
                unset($input['arquivo']);
            }

            $arquivo->update($input);

            return redirect()->route('painel.arquivos.index')->with('success', 'Arquivo alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar arquivo: '.$e->getMessage()]);

        }
    }

    public function destroy(Arquivo $arquivo)
    {
        try {

            $arquivo->delete();
            return redirect()->route('painel.arquivos.index')
                             ->with('success', 'Arquivo excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir arquivo: '.$e->getMessage()]);

        }
    }
}