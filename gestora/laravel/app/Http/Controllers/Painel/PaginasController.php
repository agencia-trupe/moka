<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PaginasRequest;
use App\Http\Controllers\Controller;

use App\Models\Pagina;
use App\Helpers\CropImage;

class PaginasController extends Controller
{
    public function index()
    {
        $paginas = Pagina::orderBy('id', 'ASC')->get();

        return view('painel.paginas.index', compact('paginas'));
    }

    public function edit(Pagina $pagina)
    {
        return view('painel.paginas.edit', compact('pagina'));
    }

    public function update(PaginasRequest $request, Pagina $pagina)
    {
        try {

            $input = array_filter($request->all(), 'strlen');

            $pagina->update($input);
            return redirect()->route('painel.paginas.index')->with('success', 'Página alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar pagina: '.$e->getMessage()]);

        }
    }

    public function imageUpload(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'imagem' => 'image|required'
        ]);

        if ($validator->fails()) {
            $response = [
                'error'   => true,
                'message' => 'O arquivo deve ser uma imagem.'
            ];
        } else {
            $imagem   = CropImage::make('imagem', [
                'width'  => 800,
                'height' => null,
                'upsize' => true,
                'path'   => 'assets/img/paginas/'
            ]);
            $response = [
                'filepath' => asset('assets/img/paginas/' . $imagem)
            ];
        }

        return response()->json($response);
    }
}
