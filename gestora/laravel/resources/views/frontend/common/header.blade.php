    <header>
        <div class="center">
            <nav id="desktop">
                <h1>
                    <a class="logo" href="{{ route('home') }}">
                        <img src="{{ asset('assets/img/layout/moka-gestora.png') }}" alt="{{ config('site.name') }}">
                    </a>
                </h1>

                @foreach($paginas as $link)
                <a class="link @if(isset($pagina) && $link->slug == $pagina->slug) active @endif" href="{{ route('pagina', $link->slug) }}">{{ $link->titulo }}</a>
                @endforeach

                <a href="#" class="link externo">Moka Invest</a>
            </nav>

            <div id="handle-mobile">
                <h1>
                    <a class="logo" href="{{ route('home') }}">
                        <img src="{{ asset('assets/img/layout/moka-gestora.png') }}" alt="{{ config('site.name') }}">
                    </a>
                </h1>

                <button id="mobile-toggle" type="button" role="button">
                    <span class="lines"></span>
                </button>
            </div>
        </div>

        <div class="login">
            <div class="center">
                <form action="http://portal.mokainvest.com.br/" method="post">
                    <input name="login" type="text" placeholder="Usuário" required>
                    <input name="senha" type="password" placeholder="Senha" required>
                    <input type="submit" value="OK">
                </form>
            </div>
        </div>
    </header>

    <nav id="mobile">
        @foreach($paginas as $link)
        <a class="link @if(isset($pagina) && $link->slug == $pagina->slug) active @endif" href="{{ route('pagina', $link->slug) }}">{{ $link->titulo }}</a>
        @endforeach

        <a href="#" class="link externo">Moka Invest</a>
    </nav>
