    <footer>
        <div class="center">
            <p><span>{{ config('site.name') }}</span> - {{ $contato->endereco }} • Tel {{ $contato->telefone }} • <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a></p>
        </div>
    </footer>
