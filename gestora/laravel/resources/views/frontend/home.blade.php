@extends('frontend.common.template')

@section('content')

    <div class="banners">
        @foreach($banners as $banner)
        <div class="banner" style="background-image: url('{{ asset('assets/img/banners/'.$banner->imagem) }}');">
            @if($banner->frase)
            <div class="frase">
                <span>{{ $banner->frase }}</span>
            </div>
            @endif
        </div>
        @endforeach
    </div>

@endsection
