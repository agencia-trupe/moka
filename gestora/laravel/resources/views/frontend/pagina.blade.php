@extends('frontend.common.template')

@section('content')

    <div class="center">
        <div class="main">
            <h1>{{ $pagina->titulo }}</h1>

            @if($pagina->slug === 'atendimento')
            <h2>{{ config('site.name') }}</h2>
            <ul>
                <li>Fone: {{ $contato->telefone }}</li>
                <li>E-mail: <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a></li>
                <li>Endereço: {{ $contato->endereco }}</li>
            </ul>
            @endif

            @if($pagina->slug === 'fundos' && count($arquivos) > 0)
                <p>Moka Fund I - FIDC</p>
                <ul style="margin-bottom:20px;">
                    @foreach($arquivos as $arquivo)
                    <li><a href="{{ url('assets/fundos/'.$arquivo->arquivo) }}" target="_blank">{{ $arquivo->titulo }}</a></li>
                    @endforeach
                </ul>
            @endif

            @if($pagina->slug === 'atendimento')
            <div class="disclosure">
                {!! $pagina->texto !!}
            </div>
            @else
                {!! $pagina->texto !!}
            @endif

            @if($pagina->slug === 'relatorios' && count($relatorios) > 0)
                @foreach($relatorios as $relatorio)
                <p><a href="{{ url('assets/relatorios/'.$relatorio->arquivo) }}" target="_blank">{{ $relatorio->titulo }}</a></p>
                @endforeach
            @endif
        </div>
    </div>

@endsection
