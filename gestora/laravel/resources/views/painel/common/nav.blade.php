<ul class="nav navbar-nav">
    <li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.banners.index') }}">Banners</a>
    </li>
    <li @if(str_is('painel.paginas*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.paginas.index') }}">Páginas</a>
    </li>
    <li @if(str_is('painel.arquivos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.arquivos.index') }}">Arquivos (Fundos)</a>
    </li>
    <li @if(str_is('painel.relatorios*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.relatorios.index') }}">Arquivos (Relatórios)</a>
    </li>
    <li @if(str_is('painel.contato*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.contato.index') }}">Contato</a>
    </li>
</ul>
