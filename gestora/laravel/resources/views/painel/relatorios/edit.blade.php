@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Editar Arquivo (Relatórios)</h2>
    </legend>

    {!! Form::model($relatorio, [
        'route'  => ['painel.relatorios.update', $relatorio->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.relatorios.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
