@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Adicionar Arquivo (Relatórios)</h2>
    </legend>

    {!! Form::open(['route' => 'painel.relatorios.store', 'files' => true]) !!}

        @include('painel.relatorios.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
