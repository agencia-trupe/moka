@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Editar Arquivo (Fundos)</h2>
    </legend>

    {!! Form::model($arquivo, [
        'route'  => ['painel.arquivos.update', $arquivo->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.arquivos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
