@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Adicionar Arquivo (Fundos)</h2>
    </legend>

    {!! Form::open(['route' => 'painel.arquivos.store', 'files' => true]) !!}

        @include('painel.arquivos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
