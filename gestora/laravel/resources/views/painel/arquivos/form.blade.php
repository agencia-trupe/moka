@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('arquivo', 'Arquivo') !!}
@if($submitText == 'Alterar' && $arquivo->arquivo)
    <a href="{{ url('assets/fundos/'.$arquivo->arquivo) }}" target="_blank" style="display:block;margin:10px 0;">{{ $arquivo->arquivo }}</a>
@endif
    {!! Form::file('arquivo', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.arquivos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
