@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Páginas
        </h2>
    </legend>

    @if(!count($paginas))
    <div class="alert alert-warning" role="alert">Nenhuma página cadastrada.</div>
    @else
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Título</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($paginas as $pagina)
            <tr class="tr-row">
                <td>{{ $pagina->titulo }}</td>
                <td class="crud-actions">
                    <a href="{{ route('painel.paginas.edit', $pagina->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
