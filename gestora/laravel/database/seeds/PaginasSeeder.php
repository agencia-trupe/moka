<?php

use Illuminate\Database\Seeder;

class PaginasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('paginas')->insert([
            [
                'slug'   => 'moka-gestora',
                'titulo' => 'Moka Gestora',
                'texto'  => 'texto',
            ],
            [
                'slug'   => 'fundos',
                'titulo' => 'Fundos',
                'texto'  => 'texto',
            ],
            [
                'slug'   => 'relatorios',
                'titulo' => 'Relatórios',
                'texto'  => 'texto',
            ],
            [
                'slug'   => 'atendimento',
                'titulo' => 'Atendimento',
                'texto'  => 'texto',
            ],
        ]);
    }
}
